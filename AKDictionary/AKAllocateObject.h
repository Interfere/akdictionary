//
//  AKAllocateObject.h
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 23/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import <CoreFoundation/CoreFoundation.h>

extern __attribute__((visibility("hidden"))) id AKAllocateObject(Class cls, size_t extraBytes);
