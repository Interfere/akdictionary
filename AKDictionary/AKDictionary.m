//
//  AKDictionary.m
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 20/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AKDictionary.h"
#import "AKDictionaryPlaceholder.h"


static const NSUInteger kDefaultCapacity = 32;

@implementation AKDictionary

@dynamic size;
@dynamic capacity;

+ (instancetype)allocWithZone:(struct _NSZone *)zone {
    return [AKDictionaryPlaceholder immutablePlaceholder];
}

- (instancetype)init {
    return [self initWithCapacity:kDefaultCapacity];
}

- (instancetype)initWithCapacity:(NSUInteger)capacity {
    return nil;
}

- (id)objectForKey:(id)aKey {
    return nil;
}

- (void)setObject:(id)anObject forKey:(id<NSCopying>)aKey {
}

- (void)removeObjectForKey:(id)aKey {
}

@end
