//
//  AKLRUCacheImpl.h
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 23/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AKDictionary.h"

@interface AKLRUCacheImpl : AKDictionary

+ (id)__new:(NSUInteger)capacity;

@end
