//
//  AKDictionaryPlaceholder.h
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 20/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AKDictionary.h"

@interface AKDictionaryPlaceholder : AKDictionary

+ (id)alloc NS_UNAVAILABLE;
+ (id)allocWithZone:(struct _NSZone *)zone NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)immutablePlaceholder;

@end
