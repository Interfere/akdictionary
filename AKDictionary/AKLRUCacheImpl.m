//
//  AKLRUCacheImpl.m
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 23/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import "AKLRUCacheImpl.h"
#import "cpl_list.h"
#import "AKAllocateObject.h"

static const CFTypeRef __AKDeletedKeyMarker = (CFTypeRef)-1;
static const NSUInteger __AKLRUCacheSizes[] = {
    0, 3, 7, 13, 23, 41, 71, 127,
    191, 251, 383, 631, 1087, 1723, 2803, 4523,
    7351, 11959, 19447, 31231, 50683, 81919, 132607, 214519,
    346607, 561109, 907759, 1468927, 2376191, 3845119, 6221311, 10066421,
    16287743, 26354171, 42641881, 68996069, 111638519, 180634607, 292272623, 472907251,
    765180413, 1238087663, 2003267557, 3241355263, 5244622819, 8485977589, 13730600407, 22216578047,
    35947178479, 58163756537, 94110934997, 152274691561, 246385626107, 398660317687, 645045943807, 1043706260983,
    1688752204787, 2732458465769, 4421210670577, 7153669136377, 11574879807461, 18728548943849, 30303428750843, 0
};

struct KeyValuePair {
    CFTypeRef aKey, anObject;
    cpl_dlist_t list_node;
};

@implementation AKLRUCacheImpl {
    NSUInteger      _szidx;
    NSUInteger      _capacity;
    NSUInteger      _size;
    cpl_dlist_t     _head;
}

+ (id)__new:(NSUInteger)capacity {
    int i = 0;
    while (__AKLRUCacheSizes[i] < capacity) { i += 1; }
    AKLRUCacheImpl *object = AKAllocateObject(self, sizeof(struct KeyValuePair) * __AKLRUCacheSizes[i]);
    object->_szidx = i;
    object->_capacity = capacity;
    cpl_dlist_init(&object->_head);
    return object;
}

- (NSUInteger)capacity {
    return _capacity;
}

- (NSUInteger)size {
    return _size;
}

- (id)objectForKey:(id)aKey {
    NSUInteger size = __AKLRUCacheSizes[_szidx];
    struct KeyValuePair *storage = (struct KeyValuePair *)object_getIndexedIvars(self);
    NSUInteger fetchIdx = [aKey hash] % size;
    
    for (int i = 0; i < size; i++) {
        id fetchedKey = (__bridge id)(storage[fetchIdx].aKey);
        
        if (fetchedKey == nil) {
            return nil;
        }
        
        if (fetchedKey != __AKDeletedKeyMarker && (fetchedKey == aKey || [fetchedKey isEqual:aKey])) {
            struct KeyValuePair *node = storage + fetchIdx;
            if (_head.next != &node->list_node) {
                cpl_dlist_del(&node->list_node);
                cpl_dlist_add_tail(&node->list_node, _head.next);
            }
            return CFBridgingRelease(node->anObject);
        }
        
        fetchIdx++;
        if (fetchIdx == size) {
            fetchIdx = 0;
        }
    }
    
    return nil;
}

- (void)setObject:(id)anObject forKey:(id<NSObject,NSCopying>)aKey {
    NSParameterAssert(aKey);
    NSParameterAssert(anObject);
    
    NSUInteger size = __AKLRUCacheSizes[_szidx];
    struct KeyValuePair *storage = (struct KeyValuePair *)object_getIndexedIvars(self);
    NSUInteger fetchIdx = [aKey hash] % size;
    
    for (int i = 0; i < size; i++) {
        id fetchedKey = (__bridge id)(storage[fetchIdx].aKey);
        
        if (fetchedKey == nil || fetchedKey == __AKDeletedKeyMarker || fetchedKey == aKey || [fetchedKey isEqual:aKey]) {
            struct KeyValuePair *node = storage + fetchIdx;
            if (self.size == self.capacity) {
                struct KeyValuePair *tail = cpl_dlist_entry(_head.prev, struct KeyValuePair, list_node);
                cpl_dlist_del(&tail->list_node);
                CFRelease(tail->anObject);
                tail->anObject = nil;
                CFRelease(tail->aKey);
                tail->aKey = __AKDeletedKeyMarker;
                _size -= 1;
            }
            cpl_dlist_add_tail(&node->list_node, _head.next);
            
            if (fetchedKey == nil || fetchedKey == __AKDeletedKeyMarker) {
                node->aKey = CFBridgingRetain([aKey copyWithZone:nil]);
            }
            
            CFTypeRef oldObject = node->anObject;
            node->anObject = CFBridgingRetain(anObject);
            if (oldObject)
                CFRelease(oldObject);
            _size += 1;
            return;
        }
        
        fetchIdx++;
        if (fetchIdx == size) {
            fetchIdx = 0;
        }
    }
}

- (void)removeObjectForKey:(id)aKey {
    NSParameterAssert(aKey);
    
    NSUInteger size = __AKLRUCacheSizes[_szidx];
    struct KeyValuePair *storage = (struct KeyValuePair *)object_getIndexedIvars(self);
    NSUInteger fetchIdx = [aKey hash] % size;
    
    for (int i = 0; i < size; i++) {
        id fetchedKey = (__bridge id)(storage[fetchIdx].aKey);
        
        if (fetchedKey == nil || fetchedKey == __AKDeletedKeyMarker) {
            return;
        }
        
        if (fetchedKey == aKey || [fetchedKey isEqual:aKey]) {
            struct KeyValuePair *node = storage + fetchIdx;
            cpl_dlist_del(&node->list_node);
            CFRelease(node->anObject);
            node->anObject = nil;
            CFRelease(node->aKey);
            node->aKey = __AKDeletedKeyMarker;
            _size -= 1;
            return;
        }
        
        fetchIdx++;
        if (fetchIdx == size) {
            fetchIdx = 0;
        }
    }
}

- (NSString *)description {
    cpl_dlist_ref iter = nil;
    NSMutableString *descr = [[NSMutableString alloc] initWithString:@"{\n"];
    cpl_dlist_foreach(iter, &_head) {
        struct KeyValuePair *node = cpl_dlist_entry(iter, struct KeyValuePair, list_node);
        [descr appendFormat:@"%@ = %@,\n", node->aKey, node->anObject];
    }
    [descr appendString:@"}"];
    return [descr copy];
}

@end
