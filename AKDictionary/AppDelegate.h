//
//  AppDelegate.h
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 20/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

