//
//  AKAllocateObject.m
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 23/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import "AKAllocateObject.h"
#import <objc/runtime.h>

id AKAllocateObject(Class cls, size_t extraBytes)
{
    return class_createInstance(cls, extraBytes);
}
