//
//  ViewController.m
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 20/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import "ViewController.h"
#import "AKDictionary.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Удалить черновик?"
                                                   message:@"Черновик будет удален безвозвратно! Вы согласны?"
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:@"Полностью согласен!", nil];
    [alert show];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:@"Object 1" forKey:@"Key 1"];
    NSLog(@"%@", dict);
    NSLog(@"%@", [dict objectForKey:@"Key 1"]);
    
    AKDictionary *placeholder = [[AKDictionary alloc] initWithCapacity:2];
    NSLog(@"%@", placeholder);
    [placeholder setObject:@"Object1" forKey:@"Key1"];
    NSLog(@"%@", placeholder);
    [placeholder setObject:@"Object2" forKey:@"Key2"];
    NSLog(@"%@", placeholder);
    [placeholder setObject:@"Object3" forKey:@"Key3"];
    NSLog(@"%@", placeholder);
    [placeholder setObject:@"Object4" forKey:@"Key4"];
    NSLog(@"%@", placeholder);
    [placeholder setObject:@"Object5" forKey:@"Key5"];
    NSLog(@"%@", placeholder);
    [placeholder setObject:@"Object6" forKey:@"Key6"];
    NSLog(@"%@", placeholder);
    [placeholder objectForKey:@"Key5"];
    NSLog(@"%@", placeholder);
    [placeholder objectForKey:@"Key5"];
    NSLog(@"%@", placeholder);
    
    [placeholder removeObjectForKey:@"Key5"];
    NSLog(@"%@", placeholder);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
