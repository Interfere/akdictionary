//
//  AKDictionaryPlaceholder.m
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 20/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import "AKDictionaryPlaceholder.h"
#import <objc/runtime.h>
#import "AKLRUCacheImpl.h"

static struct objc_object __immutablePlaceholder;

@implementation AKDictionaryPlaceholder

+ (void)initialize {
    object_setClass((id)(&__immutablePlaceholder), [self class]);
}

+ (instancetype)immutablePlaceholder {
    return (CFTypeRef)(&__immutablePlaceholder);
}

- (instancetype)retain {
    return (CFTypeRef)(&__immutablePlaceholder);
}

- (instancetype)autorelease {
    return (CFTypeRef)(&__immutablePlaceholder);
}

- (oneway void)release {}

- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

- (instancetype)initWithCapacity:(NSUInteger)capacity {
    [super initWithCapacity:capacity]; // convience compiler
    return (id)[AKLRUCacheImpl __new:capacity];
}

@end
