//
//  main.m
//  AKDictionary
//
//  Created by Alexey A. Ushakov on 20/02/16.
//  Copyright © 2016 Alexey A. Ushakov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
